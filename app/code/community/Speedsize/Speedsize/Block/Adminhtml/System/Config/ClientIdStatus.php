<?php
/**
 * SpeedSize module for Magento 1
 *
 * @category SpeedSize
 * @package  Speedsize_Speedsize
 * @author   Developer: Pniel Cohen (Trus)
 * @author   Trus (https://www.trus.co.il/)
 */

/**
 * Speedsize_Speedsize_Block_Adminhtml_System_Config_ClientIdStatus
 */
class Speedsize_Speedsize_Block_Adminhtml_System_Config_ClientIdStatus extends Speedsize_Speedsize_Block_Adminhtml_System_Config_AbstractField
{
    /**
     * @param  Varien_Data_Form_Element_Abstract $element
     * @return string
     */
    public function render(Varien_Data_Form_Element_Abstract $element)
    {
        if (!$this->getSpeedSizeClientId()) {
            return '';
        } else {
            $this->_speedsizeApi->refreshSpeedSizeClientIdStatuses();
        }
        return parent::render($element);
    }

    /**
     * Return element html
     * @param  Varien_Data_Form_Element_Abstract $element
     * @return string
     */
    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
        $speedsizeClientIdStatus = $this->getSpeedSizeClientIdStatus(true);
        return '<span style="color:' . ($speedsizeClientIdStatus ? '#79a22e' : '#e22626') . ';">' .
                __($speedsizeClientIdStatus ? 'Active' : 'Inactive') .
            '</span>';
    }
}
