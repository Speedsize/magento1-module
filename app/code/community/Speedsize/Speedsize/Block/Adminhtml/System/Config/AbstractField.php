<?php
/**
 * SpeedSize module for Magento 1
 *
 * @category SpeedSize
 * @package  Speedsize_Speedsize
 * @author   Developer: Pniel Cohen (Trus)
 * @author   Trus (https://www.trus.co.il/)
 */

/**
 * Speedsize_Speedsize_Block_Adminhtml_System_Config_AbstractField
 */
class Speedsize_Speedsize_Block_Adminhtml_System_Config_AbstractField extends Mage_Adminhtml_Block_System_Config_Form_Field
{
    /**
     * @var Speedsize_Speedsize_Helper_Data
     */
    protected $_speedsizeHelper;

    /**
     * @var Speedsize_Speedsize_Helper_Api
     */
    protected $_speedsizeApi;

    /**
     * @var Varien_Object
     */
    protected $_scopeInfo;

    /**
     * @method _construct
     */
    protected function _construct()
    {
        parent::_construct();
        $this->_speedsizeHelper = Mage::helper('speedsize_speedsize/data');
        $this->_speedsizeApi = Mage::helper('speedsize_speedsize/api');
        $this->_scopeInfo = $this->_speedsizeHelper->getScopeInfo();
    }

    /**
     * @param Varien_Data_Form_Element_Abstract $element
     * @return string
     */
    public function render(Varien_Data_Form_Element_Abstract $element)
    {
        $element->unsScope()->unsCanUseWebsiteValue()->unsCanUseDefaultValue();
        return parent::render($element);
    }

    /**
     * Return element html
     *
     * @param  Varien_Data_Form_Element_Abstract $element
     * @return string
     */
    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
        return parent::_getElementHtml($element);
    }

    /**
     * @method isEnabled
     * @return string
     */
    public function isEnabled()
    {
        return $this->_speedsizeHelper->isEnabled($this->_scopeInfo['scope'], $this->_scopeInfo['store_id']);
    }

    /**
     * @method getSpeedSizeClientId
     * @return string
     */
    public function getSpeedSizeClientId()
    {
        return $this->_speedsizeHelper->getSpeedSizeClientId($this->_scopeInfo['scope'], $this->_scopeInfo['store_id']);
    }

    /**
     * @method getSpeedSizeClientIdStatus
     * @param  boolean                    $returnIsActive
     * @return mixed
     */
    public function getSpeedSizeClientIdStatus($returnIsActive = false)
    {
        return $this->_speedsizeApi->getSpeedSizeClientIdStatus($returnIsActive, 'stores', $this->_scopeInfo['store_id']);
    }
}
