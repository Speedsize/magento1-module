<?php
/**
 * SpeedSize module for Magento 1
 *
 * @category SpeedSize
 * @package  Speedsize_Speedsize
 * @author   Developer: Pniel Cohen (Trus)
 * @author   Trus (https://www.trus.co.il/)
 */

/**
 * Speedsize_Speedsize_Block_Adminhtml_System_Config_Version
 */
class Speedsize_Speedsize_Block_Adminhtml_System_Config_Version extends Speedsize_Speedsize_Block_Adminhtml_System_Config_AbstractField
{
    /**
     * Return element html
     *
     * @param  Varien_Data_Form_Element_Abstract $element
     * @return string
     */
    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
        return "<div>{$this->_speedsizeHelper->getModuleVersion()}</div>";
    }
}
