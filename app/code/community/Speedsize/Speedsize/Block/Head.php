<?php
/**
 * SpeedSize module for Magento 1
 *
 * @category SpeedSize
 * @package  Speedsize_Speedsize
 * @author   Developer: Pniel Cohen (Trus)
 * @author   Trus (https://www.trus.co.il/)
 */

/**
 * Speedsize_Speedsize_Block_Head
 */
class Speedsize_Speedsize_Block_Head extends Mage_Core_Block_Template
{
    /**
     * @var Speedsize_Speedsize_Helper_Processor
     */
    protected $speedsizeProcessor;

    /**
     * Initialize template
     *
     */
    protected function _construct()
    {
        parent::_construct();
        $this->speedsizeProcessor = Mage::helper('speedsize_speedsize/processor');
    }

    public function canProcess()
    {
        return $this->speedsizeProcessor->canProcess();
    }

    public function getSpeedSizeServiceUrl()
    {
        return Speedsize_Speedsize_Helper_Data::SPEEDSIZE_SERVICE_URL;
    }
}
