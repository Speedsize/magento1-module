<?php
/**
 * SpeedSize module for Magento 1
 *
 * @category SpeedSize
 * @package  Speedsize_Speedsize
 * @author   Developer: Pniel Cohen (Trus)
 * @author   Trus (https://www.trus.co.il/)
 */

/**
 * Speedsize_Speedsize_Model_Catalog_Product_Media_Config
 */
class Speedsize_Speedsize_Model_Catalog_Product_Media_Config extends Mage_Catalog_Model_Product_Media_Config
{
    /**
     * @var Speedsize_Speedsize_Helper_Processor
     */
    private $speedsizeProcessor;

    /**
     * @return Speedsize_Speedsize_Helper_Processor
     */
    private function getSpeedsizeProcessor()
    {
        return is_null($this->speedsizeProcessor) ? Mage::helper('speedsize_speedsize/processor') : $this->speedsizeProcessor;
    }

    /**
     * @param string $file relative image filepath
     * @return string
     */
    public function getMediaUrl($file)
    {
        $url = parent::getMediaUrl($file);

        try {
            $speedsizeProcessor = $this->getSpeedsizeProcessor();
            if (!$speedsizeProcessor->canProcess() || $speedsizeProcessor->isSpeedSizeUrl($url) || !$url) {
                return $url;
            }
            if (
                $speedsizeProcessor->getSpeedsizeHelper()->isSpeedSizeParserImageSizeParamsEnabled() &&
                ($image = $speedsizeProcessor->getSpeedsizeHelper()->separateSizesFromCacheImageUrl($url)) &&
                !empty($image['width'])
            ) {
                $height = !empty($image['height']) ? $image['height'] : $image['width'];
                $params = array(
                    "w_{$image['width']}",
                    "h_{$height}",
                    "r_contain"
                );
                return $speedsizeProcessor->prefixUrl($image['url'], $params, false);
            } else {
                return $speedsizeProcessor->prefixUrl($url, array(), false);
            }
        } catch (\Exception $e) {
            return $url;
        }
    }
}
