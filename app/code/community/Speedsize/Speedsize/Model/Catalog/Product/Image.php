<?php
/**
 * SpeedSize module for Magento 1
 *
 * @category SpeedSize
 * @package  Speedsize_Speedsize
 * @author   Developer: Pniel Cohen (Trus)
 * @author   Trus (https://www.trus.co.il/)
 */

/**
 * Speedsize_Speedsize_Model_Catalog_Product_Image
 */
class Speedsize_Speedsize_Model_Catalog_Product_Image extends Mage_Catalog_Model_Product_Image
{
    /**
     * @var Speedsize_Speedsize_Helper_Processor
     */
    private $speedsizeProcessor;

    /**
     * @return Speedsize_Speedsize_Helper_Processor
     */
    private function getSpeedsizeProcessor()
    {
        return is_null($this->speedsizeProcessor) ? Mage::helper('speedsize_speedsize/processor') : $this->speedsizeProcessor;
    }

    public function getUrl()
    {
        $url = parent::getUrl();

        try {
            $speedsizeProcessor = $this->getSpeedsizeProcessor();

            if (!$speedsizeProcessor->canProcess() || $speedsizeProcessor->isSpeedSizeUrl($url) || !$url) {
                return $url;
            }

            $origUrl = Mage::getBaseUrl('media') . str_replace(DS, '/', str_replace(Mage::getBaseDir('media') . DS, "", $this->getBaseFile()));

            $width = (string) $this->getWidth();
            $params = !empty($width) ? array("w_{$width}") : array();
            if ($this->_keepFrame && $width) {
                $height = (string) $this->getHeight() ?: $width;
                $params[] = "h_{$height}";
                $params[] = "r_contain";
            }

            return $speedsizeProcessor->prefixUrl(
                $origUrl,
                $params,
                false
            );
        } catch (\Exception $e) {
            return $url;
        }
    }

    /**
     * Expose _keepFrame from parent model
     * @method getKeepFrameVal
     * @return bool
     */
    public function getKeepFrameVal()
    {
        return $this->_keepFrame;
    }
}
