<?php
/**
 * SpeedSize module for Magento 1
 *
 * @category SpeedSize
 * @package  Speedsize_Speedsize
 * @author   Developer: Pniel Cohen (Trus)
 * @author   Trus (https://www.trus.co.il/)
 */

/**
 * Speedsize_Speedsize_Model_Observer_Config
 */
class Speedsize_Speedsize_Model_Observer_Config
{
    /**
     * @var Speedsize_Speedsize_Helper_Data
     */
    private $speedsizeHelper;

    /**
     * @var Speedsize_Speedsize_Helper_Api
     */
    private $speedsizeApi;

    /**
     * @var Varien_Object
     */
    private $scopeInfo;

    /**
     * @method __construct
     */
    public function __construct()
    {
        $this->speedsizeHelper = Mage::helper('speedsize_speedsize/data');
        $this->speedsizeApi = Mage::helper('speedsize_speedsize/api');
        $this->scopeInfo = $this->speedsizeHelper->getScopeInfo();
    }

    /**
     * {@inheritdoc}
     */
    public function speedsizeHelperChanged(Varien_Event_Observer $observer)
    {
        $this->cleanConfigCache();

        $scope = $this->scopeInfo['scope'];
        $scopeId = $scope === 'websites' ? $this->scopeInfo['website_id'] : $this->scopeInfo['store_id'];

        if ($this->speedsizeHelper->isEnabled($scope, $scopeId)) {
            $speedsizeClientId = $this->speedsizeHelper->getSpeedSizeClientId($scope, $scopeId);
            if (!$speedsizeClientId) {
                $this->speedsizeHelper->updateIsEnabled(false, $scope, $scopeId);
                throw new Mage_Core_Exception(
                    __('SpeedSize ID is required for enabling SpeedSize!')
                );
            }

            $isValidClientId = $this->speedsizeApi->getSpeedSizeClientIdStatus(true, $scope, $scopeId);
            if (!$isValidClientId) {
                throw new Mage_Core_Exception(
                    __('SpeedSize ID is invalid, inactive or doesn\'t exist!')
                );
            }

            $this->speedsizeApi->refreshSpeedSizeClientIdStatuses();

            Mage::getSingleton('adminhtml/session')->addSuccess(__('SpeedSize ID is active!'));

            return $this;
        }
    }

    private function cleanConfigCache()
    {
        try {
            Mage::app()->getCacheInstance()->cleanType("config");
            Mage::dispatchEvent('adminhtml_cache_refresh_type', array('type' => "config"));
            Mage::getConfig()->reinit();
        } catch (\Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError(__('For some reason, SpeedSize couldn\'t clear your config cache, please clear the cache manually. (Exception message: %1)', $e->getMessage()));
        }
        return $this;
    }
}
