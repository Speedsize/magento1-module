<?php
/**
 * SpeedSize module for Magento 1
 *
 * @category SpeedSize
 * @package  Speedsize_Speedsize
 * @author   Developer: Pniel Cohen (Trus)
 * @author   Trus (https://www.trus.co.il/)
 */

/**
 * Speedsize_Speedsize_Model_Observer_Front
 */
class Speedsize_Speedsize_Model_Observer_Front
{
    /**
     * Prefix for cache key
     */
    public const CACHE_KEY_PREFIX = 'SPEEDSIZE_';

    /**
     * Cache group Tag
     */
    public const CACHE_GROUP = 'speedsize_block_html';

    /**
     * Application instance
     *
     * @var Mage_Core_Model_App
     */
    private $app;

    /**
     * @var Speedsize_Speedsize_Helper_Parser
     */
    private $speedsizeParser;

    /**
     * @var Speedsize_Speedsize_Helper_Processor
     */
    private $speedsizeProcessor;

    /**
     * @return Mage_Core_Model_App
     */
    private function getApp()
    {
        return is_null($this->app) ? Mage::app() : $this->_app;
    }

    /**
     * @return Speedsize_Speedsize_Helper_Parser
     */
    private function getSpeedsizeParser()
    {
        return is_null($this->speedsizeParser) ? Mage::helper('speedsize_speedsize/parser') : $this->speedsizeParser;
    }

    /**
     * @return Speedsize_Speedsize_Helper_Processor
     */
    private function getSpeedsizeProcessor()
    {
        return is_null($this->speedsizeProcessor) ? Mage::helper('speedsize_speedsize/processor') : $this->speedsizeProcessor;
    }

    /**
     * Load block html from cache storage
     * @param Mage_Core_Block_Abstract $block
     * @return string | false
     */
    private function loadCache($block)
    {
        if (is_null($block->getCacheLifetime()) || !$this->getApp()->useCache(self::CACHE_GROUP)) {
            return false;
        }

        $cacheKey = self::CACHE_KEY_PREFIX . $block->getCacheKey();
        /** @var $session Mage_Core_Model_Session */
        $session = Mage::getSingleton('core/session');
        $cacheData = $this->getApp()->loadCache($cacheKey);
        if ($cacheData) {
            $cacheData = str_replace(
                '<!--SID=' . $cacheKey . '-->',
                $session->getSessionIdQueryParam() . '=' . $session->getEncryptedSessionId(),
                $cacheData
            );
        }
        return $cacheData;
    }

    /**
     * Save block content to cache storage
     * @param Mage_Core_Block_Abstract $block
     * @param string $data
     * @return Speedsize_Speedsize_Model_Observer_Front
     */
    private function saveCache($block, $data)
    {
        if (is_null($block->getCacheLifetime()) || !$this->getApp()->useCache(self::CACHE_GROUP)) {
            return false;
        }
        $cacheKey = self::CACHE_KEY_PREFIX . $block->getCacheKey();
        /** @var $session Mage_Core_Model_Session */
        $session = Mage::getSingleton('core/session');
        $data = str_replace(
            $session->getSessionIdQueryParam() . '=' . $session->getEncryptedSessionId(),
            '<!--SID=' . $cacheKey . '-->',
            $data
        );

        $tags = $block->getCacheTags();
        if (!in_array(self::CACHE_GROUP, $tags)) {
            $tags[] = self::CACHE_GROUP;
        }

        $this->getApp()->saveCache($data, $cacheKey, $tags, $block->getCacheLifetime());
        $this->getApp()->saveCache(json_encode($tags), md5($cacheKey . '_tags'), $tags, $block->getCacheLifetime());

        return $this;
    }

    /**
     * @method coreBlockAbstractToHtmlAfter
     * @param  Varien_Event_Observer $observer
     */
    public function coreBlockAbstractToHtmlAfter(Varien_Event_Observer $observer)
    {
        try {
            if (!$this->getSpeedsizeParser()->canParse()) {
                return;
            }

            /** @var $block Mage_Core_Block_Abstract */
            $block = $observer->getEvent()->getBlock();
            /** @var $transport Varien_Object */
            $transport = $observer->getEvent()->getTransport();

            $html = $this->loadCache($block);
            if ($html === false) {
                //Parse and wrap URLs
                $html = $this->getSpeedsizeParser()->wrapUrls(
                    $transport->getHtml()
                );
                //Save to cache
                $this->saveCache($block, $html);
            }
            $transport->setHtml($html);
        } catch (\Exception $e) {
            // Ignore errors and continue
        }
    }

    /**
     * @method controllerFrontSendResponseBefore
     * @param  Varien_Event_Observer             $observer
     */
    public function controllerFrontSendResponseBefore(Varien_Event_Observer $observer)
    {
        try {
            if (!$this->getSpeedsizeProcessor()->canProcess()) {
                return;
            }

            $response = $observer->getEvent()->getFront()->getResponse();

            //Add response headers
            $headers = array_map('strtolower', array_column((array)$response->getHeaders(), 'name'));
            if (!array_search('accept-ch', $headers)) {
                $response->setHeader('Accept-CH', 'Viewport-Width, Width, DPR');
            }
            if (!array_search('feature-policy', $headers)) {
                $response->setHeader('Feature-Policy', \sprintf('ch-viewport-width %s, ch-dpr %s, ch-width %s', Speedsize_Speedsize_Helper_Data::SPEEDSIZE_SERVICE_URL, Speedsize_Speedsize_Helper_Data::SPEEDSIZE_SERVICE_URL, Speedsize_Speedsize_Helper_Data::SPEEDSIZE_SERVICE_URL));
            }
        } catch (\Exception $e) {
            // Ignore errors and continue
        }
    }
}
