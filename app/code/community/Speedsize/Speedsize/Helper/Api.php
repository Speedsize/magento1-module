<?php
/**
 * SpeedSize module for Magento 1
 *
 * @category SpeedSize
 * @package  Speedsize_Speedsize
 * @author   Developer: Pniel Cohen (Trus)
 * @author   Trus (https://www.trus.co.il/)
 */

/**
 * SpeedSize API helper.
 */
class Speedsize_Speedsize_Helper_Api extends Mage_Core_Helper_Abstract
{
    /**
     * @var array
     */
    private $cache = array();

    /**
     * @var Speedsize_Speedsize_Helper_Data
     */
    protected $speedsizeHelper;

    /**
     * @method __construct
     */
    public function __construct()
    {
        $this->speedsizeHelper = Mage::helper('speedsize_speedsize/data');
    }

    public function getCurlClient()
    {
        return new Varien_Http_Adapter_Curl();
    }

    /**
     * @method getSpeedSizeClientIdStatus
     * @param  bool                       $returnIsActive
     * @param  string                     $scope
     * @param  null|int                   $scopeId
     * @param  bool                       $refresh
     * @return mixed
     */
    public function getSpeedSizeClientIdStatus($returnIsActive = false, $scope = 'stores', $scopeId = null, $refresh = false)
    {
        try {
            $speedsizeClientId = $this->speedsizeHelper->getSpeedSizeClientId($scope, $scopeId);

            if ($speedsizeClientId) {
                if (!isset($this->cache['client_id_status_' . $speedsizeClientId]) || $refresh) {
                    $curlClient = new Varien_Http_Adapter_Curl();
                    $curlClient->write('GET', $this->speedsizeHelper->getSpeedSizeApiUrl("clients/{$speedsizeClientId}"), '1.1', array("Accept" => "application/json", "Expect" => ''));
                    $response = $curlClient->read();
                    $response = Zend_Http_Response::extractBody($response);
                    $this->cache['client_id_status_' . $speedsizeClientId] = json_decode($response, true);
                }
                $response = $this->cache['client_id_status_' . $speedsizeClientId];

                if (isset($response["clientId"]) && $response["clientId"] === $speedsizeClientId && isset($response["status"])) {
                    $this->speedsizeHelper->updateSpeedSizeClientIdActive(true, $scope, $scopeId);
                    return $returnIsActive ? $response['status'] === 'Active' : $response['status'];
                }
            } elseif ($this->speedsizeHelper->isEnabled($scope, $scopeId)) {
                $this->speedsizeHelper->updateIsEnabled(false, $scope, $scopeId);
            }
        } catch (\Exception $e) {
            //Ignore exceptions
        }

        $this->speedsizeHelper->updateSpeedSizeClientIdActive(false, $scope, $scopeId);

        return $returnIsActive ? false : null;
    }

    /**
     * @method refreshSpeedSizeClientIdStatuses
     */
    public function refreshSpeedSizeClientIdStatuses()
    {
        foreach ($this->speedsizeHelper->getStores(true) as $key => $store) {
            $this->getSpeedSizeClientIdStatus(true, 'stores', $store->getId());
        }
    }
}
