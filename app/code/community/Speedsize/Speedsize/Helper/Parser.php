<?php
/**
 * SpeedSize module for Magento 1
 *
 * @category SpeedSize
 * @package  Speedsize_Speedsize
 * @author   Developer: Pniel Cohen (Trus)
 * @author   Trus (https://www.trus.co.il/)
 */

/**
 * SpeedSize Parser helper.
 */
class Speedsize_Speedsize_Helper_Parser extends Mage_Core_Helper_Abstract
{
    /**
     * @var array
     */
    private $cache = array();

    /**
     * Supported attributes pattern (with/without data- prefix):
     * src / srcset / href / poster
     * @var string
     */
    public const SUPPORTED_HTML_ATTRIBUTES_PATTERN = 'src(?:set)?|href|poster|bg(?:set)?(?:\-image)?|product\-(?:image|video|media)';

    /**
     * Supported image file extensions
     * @var string
     */
    private const IMAGE_FILE_EXTENSIONS = 'gif|jpe?g|png|tiff?|webp|jpe|bmp';

    /**
     * Supported video file extensions
     * @var string
     */
    private const VIDEO_FILE_EXTENSIONS = 'mp4|avi|mpg|rm|mov|asf|3gp|mkv|rmvb|m4v|webm|ogv';

    /**
     * Parser regex patterns
     * @var string
     */
    public const SEARCH_PATTERNS = array(
        'all_supported_html_attributes' => '/((?:\s|\-)(?:' . self::SUPPORTED_HTML_ATTRIBUTES_PATTERN . ')\s*\=\s*)(?:(\"|\')((?:(?!\2).)+?)(\2))/msi',
        'all_css_background_image_url' => '/(background(?:\-image)?\s*:[^;\{\}]*(?:url\s*\(\s*))(?:(\"|\')?((?:(?!\2|\)).)+?)(\2)?)(\s*\))/msi',
    );

    private const INJECT_BY_STORE_URL_PATTERN = '/(?<=^|[,\"\'\(\s\<\>])(?:(?:(?:https?\:)?\/\/)(?:%s)\/|\/[\w\-])[^,\"\'\)\s\<\>]+\.(?:' . self::IMAGE_FILE_EXTENSIONS . '|' . self::VIDEO_FILE_EXTENSIONS . ')(?:\?[^,\"\'\)\s\<\>]*)?/msi';

    /**
     * @var Speedsize_Speedsize_Helper_Data
     */
    private $speedsizeHelper;

    /**
     * @var Speedsize_Speedsize_Helper_Processor
     */
    protected $speedsizeProcessor;

    /**
     * @method __construct
     */
    public function __construct()
    {
        $this->speedsizeHelper = Mage::helper('speedsize_speedsize/data');
        $this->speedsizeProcessor = Mage::helper('speedsize_speedsize/processor');
    }

    /**
     * @method canParse
     * @param  bool     $refresh
     * @return bool
     */
    public function canParse($refresh = false)
    {
        if (!isset($this->cache['can_parse']) || $refresh) {
            $this->cache['can_parse'] =
                $this->speedsizeProcessor->canProcess() &&
                $this->speedsizeHelper->isSpeedSizeRealtimeParsingEnabled();
        }
        return $this->cache['can_parse'];
    }

    /**
     * @method getInjectByStoreUrlPattern
     * @param  boolean        $refresh
     * @return string
     */
    public function getInjectByStoreUrlPattern($refresh = false)
    {
        if (!isset($this->cache['INJECT_BY_STORE_URL_PATTERN']) || $refresh) {
            $storeUrlDomain = preg_replace('/^(?:https?\:)?\/\//', '', rtrim((string) $this->speedsizeHelper->getStoreLinkUrl(), '/'));
            $storeMediaUrlDomain = preg_replace('/^(?:https?\:)?\/\//', '', rtrim((string) $this->speedsizeHelper->getStoreMediaUrl(), '/'));
            $storeSkinUrlDomain = preg_replace('/^(?:https?\:)?\/\//', '', rtrim((string) $this->speedsizeHelper->getStoreSkinUrl(), '/'));
            $supportedDomains = preg_quote($storeUrlDomain, '/');
            if (strpos($storeMediaUrlDomain, $storeUrlDomain) !== 0) {
                $supportedDomains .= '|' . preg_quote($storeMediaUrlDomain, '/');
            }
            if (strpos($storeSkinUrlDomain, $storeUrlDomain) !== 0) {
                $supportedDomains .= '|' . preg_quote($storeSkinUrlDomain, '/');
            }
            $this->cache['INJECT_BY_STORE_URL_PATTERN'] = sprintf(self::INJECT_BY_STORE_URL_PATTERN, $supportedDomains);
        }
        return $this->cache['INJECT_BY_STORE_URL_PATTERN'];
    }

    /**
     * @method wrapUrls
     * @param  string $html
     * @return string
     */
    public function wrapUrls($html)
    {
        foreach (self::SEARCH_PATTERNS as $type => $searchPattern) {
            $html = preg_replace_callback(
                $searchPattern,
                function ($matches) use ($type) {
                    $matches[3] = preg_replace_callback(
                        $this->getInjectByStoreUrlPattern(),
                        function ($urlMatches) {
                            //If captcha image (skip):
                            if (strpos($urlMatches[0], '/captcha/') !== false) {
                                return $urlMatches[0];
                            }
                            //If relative path:
                            if (substr($urlMatches[0], 0, 1) === '/' && substr($urlMatches[0], 1, 1) !== '/') {
                                $urlMatches[0] = $this->speedsizeHelper->getStoreUrl($urlMatches[0]);
                            }
                            //If with domain:
                            if (
                                $this->speedsizeHelper->isSpeedSizeParserImageSizeParamsEnabled() &&
                                ($image = $this->speedsizeHelper->separateSizesFromCacheImageUrl($urlMatches[0])) &&
                                !empty($image['width'])
                            ) {
                                $height = !empty($image['height']) ? $image['height'] : $image['width'];
                                $params = array(
                                    "w_{$image['width']}",
                                    "h_{$height}",
                                    "r_contain"
                                );
                                return $this->speedsizeProcessor->prefixUrl($image['url'], $params);
                            } else {
                                return $this->speedsizeProcessor->prefixUrl($urlMatches[0]);
                            }
                        },
                        $matches[3]
                    );
                    array_shift($matches);
                    return implode("", $matches);
                },
                $html
            );
        }

        return $html;
    }
}
