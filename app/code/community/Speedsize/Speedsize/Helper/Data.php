<?php
/**
 * SpeedSize module for Magento 1
 *
 * @category SpeedSize
 * @package  Speedsize_Speedsize
 * @author   Developer: Pniel Cohen (Trus)
 * @author   Trus (https://www.trus.co.il/)
 */

/**
 * SpeedSize helper
 */
class Speedsize_Speedsize_Helper_Data extends Mage_Core_Helper_Abstract
{
    public const MODULE_NAME = 'Speedsize_Speedsize';
    public const SPEEDSIZE_SERVICE_URL = 'https://cdn.speedsize.com';
    public const SPEEDSIZE_API_URL = 'https://api.speedsize-aws.com/api';
    public const SPEEDSIZE_BYPASS_QUERY_PARAM = 'nospeedsize';

    // Config paths
    public const CONFIGPATH_SPEEDSIZE_ENABLED = 'speedsize/general_settings/enabled';
    public const CONFIGPATH_SPEEDSIZE_CLIENT_ID = 'speedsize/general_settings/speedsize_client_id';
    public const CONFIGPATH_SPEEDSIZE_CLIENT_ID_ACTIVE = 'speedsize/general_settings/speedsize_client_id_active';
    public const CONFIGPATH_SPEEDSIZE_REALTIME_PARSING_ENABLED = 'speedsize/advanced_settings/speedsize_realtime_parsing_enabled';
    public const CONFIGPATH_SPEEDSIZE_PARSER_IMAGE_SIZE_PARAMS_ENABLED = 'speedsize/advanced_settings/speedsize_parser_image_size_params_enabled';

    /**
     * @var array
     */
    private $cache = array();

    /**
     * @method getStores
     * @param  boolean   $withDefault
     * @param  boolean   $codeKey
     * @return array
     */
    public function getStores($withDefault = false, $codeKey = false)
    {
        return Mage::app()->getStores($withDefault, $codeKey);
    }

    /**
     * @method getCurrentStore
     */
    public function getCurrentStore()
    {
        return Mage::app()->getStore();
    }

    /**
     * @method getCurrentStoreId
     * @return int
     */
    public function getCurrentStoreId()
    {
        return Mage::app()->getStore()->getId();
    }

    /**
     * @method getDefaultStoreId
     * @return int
     */
    public function getDefaultStoreId()
    {
        return Mage_Catalog_Model_Abstract::DEFAULT_STORE_ID;
    }

    /**
     * @method isCurrentlySecure
     * @return bool
     */
    public function isCurrentlySecure()
    {
        return Mage::app()->getStore()->isCurrentlySecure();
    }

    /**
     * @method getModuleVersion
     * @return string
     */
    public function getModuleVersion()
    {
        return Mage::getConfig()->getModuleConfig('Speedsize_Speedsize')->version;
    }

    /**
     * Is admin area
     * @method isAdmin
     * @return boolean
     */
    public function isAdmin()
    {
        if (Mage::app()->getStore()->isAdmin()) {
            return true;
        }

        if (Mage::getDesign()->getArea() == 'adminhtml') {
            return true;
        }

        return false;
    }

    /**
     * @method getScopeInfo
     * @return array
     */
    public function getScopeInfo()
    {
        $scopeInfo = array(
            'website_id' => $this->getCurrentStore()->getWebsiteId(),
            'store_id' => $this->getCurrentStoreId(),
            'scope' => 'stores'
        );

        if (strlen($code = Mage::getSingleton('adminhtml/config_data')->getStore())) {
            $store = Mage::getModel('core/store')->load($code);
            $scopeInfo['website_id'] = $store->getId();
            $scopeInfo['store_id'] = $store->getWebsiteId();
        } elseif (strlen($code = Mage::getSingleton('adminhtml/config_data')->getWebsite())) {
            $scopeInfo['scope'] = 'websites';
            $scopeInfo['website_id'] = Mage::getModel('core/website')->load($code)->getId();
            $scopeInfo['store_id'] = Mage::app()->getWebsite($scopeInfo['website_id'])->getDefaultStore()->getId();
        } else {
            $scopeInfo['scope'] = 'default';
            $scopeInfo['store_id'] = $this->getDefaultStoreId();
        }

        return $scopeInfo;
    }

    /**
     * Return config field value.
     *
     * @param string $fieldKey Field key.
     * @param string $scope Scope.
     * @param int    $scopeId Scope ID.
     *
     * @return mixed
     */
    private function getConfigValue($fieldKey, $scope = 'stores', $scopeId = null)
    {
        if (!$scopeId && Mage::app()->isSingleStoreMode()) {
            return Mage::getStoreConfig($fieldKey);
        }
        if ($scope === 'websites') {
            return Mage::app()->getWebsite($scopeId)->getConfig($fieldKey);
        }
        return Mage::getStoreConfig(
            $fieldKey,
            is_null($scopeId) ? $this->getCurrentStoreId() : $scopeId
        );
    }

    private function setStoreConfig($path, $value, $scope = 'default', $scopeId = 0)
    {
        Mage::getModel('core/config')->saveConfig($path, $value, $scope, $scopeId)->reinit();
    }

    private function deleteStoreConfig($path, $scope = 'default', $scopeId = 0)
    {
        Mage::getModel('core/config')->deleteConfig($path, $scope, $scopeId)->reinit();
    }

    /**
     * @method resetCredentials
     * @param  string                $scope Scope
     * @param  int|null              $scopeId
     * @return Speedsize_Speedsize_Helper_Data
     */
    public function resetCredentials($scope = 'default', $scopeId = 0)
    {
        $this->deleteStoreConfig(self::CONFIGPATH_SPEEDSIZE_ENABLED, $scope, $scopeId);
        $this->deleteStoreConfig(self::CONFIGPATH_SPEEDSIZE_CLIENT_ID, $scope, $scopeId);
        $this->deleteStoreConfig(self::CONFIGPATH_SPEEDSIZE_CLIENT_ID_ACTIVE, $scope, $scopeId);
        return $this;
    }

    /**
     * Is module enabled? (field value)
     * @param  string $scope Scope.
     * @param  int    $scopeId Store ID.
     * @return bool
     */
    public function isEnabled($scope = 'stores', $scopeId = null)
    {
        return (bool) $this->getConfigValue(self::CONFIGPATH_SPEEDSIZE_ENABLED, $scope, $scopeId);
    }

    /**
     * Return SpeedSize Client ID.
     * @param  string $scope Scope.
     * @param  int    $scopeId Store ID.
     * @return string
     */
    public function getSpeedSizeClientId($scope = 'stores', $scopeId = null)
    {
        return (string) $this->getConfigValue(self::CONFIGPATH_SPEEDSIZE_CLIENT_ID, $scope, $scopeId);
    }

    /**
     * @method isSpeedSizeRealtimeParsingEnabled
     * @param  string $scope Scope.
     * @param  int    $scopeId Store ID.
     * @return int
     */
    public function isSpeedSizeRealtimeParsingEnabled($scope = 'stores', $scopeId = null)
    {
        return (bool) $this->getConfigValue(self::CONFIGPATH_SPEEDSIZE_REALTIME_PARSING_ENABLED, $scope, $scopeId);
    }

    /**
     * @method isSpeedSizeParserImageSizeParamsEnabled
     * @param  string $scope Scope.
     * @param  int    $scopeId Store ID.
     * @return int
     */
    public function isSpeedSizeParserImageSizeParamsEnabled($scope = 'stores', $scopeId = null)
    {
        return (bool) $this->getConfigValue(self::CONFIGPATH_SPEEDSIZE_PARSER_IMAGE_SIZE_PARAMS_ENABLED, $scope, $scopeId);
    }

    /**
     * Is SpeedSize Client ID active?
     * @param  string $scope Scope.
     * @param  int    $scopeId Store ID.
     * @return bool
     */
    public function getSpeedSizeClientIdActive($scope = 'stores', $scopeId = null)
    {
        return (bool) $this->getConfigValue(self::CONFIGPATH_SPEEDSIZE_CLIENT_ID_ACTIVE, $scope, $scopeId);
    }

    /**
     * @method updateIsEnabled
     * @param  bool|null             $value
     * @param  string                $scope Scope
     * @param  int|null              $scopeId Scope ID.
     */
    public function updateIsEnabled($value = null, $scope = 'stores', $scopeId = null)
    {
        $scopeId = is_null($scopeId) ? ($scope === 'default' ? $this->getDefaultStoreId() : $this->getCurrentStoreId()) : $scopeId;
        if ($value === null) {
            $this->deleteStoreConfig(self::CONFIGPATH_SPEEDSIZE_ENABLED, $scope, $scopeId);
        } else {
            $this->setStoreConfig(self::CONFIGPATH_SPEEDSIZE_ENABLED, $value ? 1 : 0, $scope, $scopeId);
        }
        return $this;
    }

    /**
     * @method updateSpeedSizeClientIdActive
     * @param  bool|null             $value
     * @param  string                $scope Scope
     * @param  int|null              $scopeId Scope ID.
     */
    public function updateSpeedSizeClientIdActive($value = null, $scope = 'stores', $scopeId = null)
    {
        $scopeId = is_null($scopeId) ? ($scope === 'default' ? $this->getDefaultStoreId() : $this->getCurrentStoreId()) : $scopeId;
        if ($value === null) {
            $this->deleteStoreConfig(self::CONFIGPATH_SPEEDSIZE_CLIENT_ID_ACTIVE, $scope, $scopeId);
        } else {
            $this->setStoreConfig(self::CONFIGPATH_SPEEDSIZE_CLIENT_ID_ACTIVE, $value ? 1 : 0, $scope, $scopeId);
        }
        return $this;
    }

    /**
     * @method getSpeedSizeServiceUrl
     * @param string $path
     * @return string
     */
    public function getSpeedSizeServiceUrl($path = "")
    {
        return self::SPEEDSIZE_SERVICE_URL . '/' . (string) $path;
    }

    /**
     * @method getSpeedSizeApiUrl
     * @param string $path
     * @return string
     */
    public function getSpeedSizeApiUrl($path = "")
    {
        return self::SPEEDSIZE_API_URL . '/' . (string) $path;
    }

    /**
     * @param  string                $scope Scope
     * @param  int|null              $scopeId Scope ID.
     * @return bool
     */
    public function hasActiveCredentials($scope = 'stores', $scopeId = null)
    {
        if (
           $this->getSpeedSizeClientId($scope, $scopeId) &&
           $this->getSpeedSizeClientIdActive($scope, $scopeId)
       ) {
            return true;
        }
        return false;
    }

    /**
     * @method getStoreUrl
     * @param  string      $path
     * @return string
     */
    public function getStoreUrl($path = '')
    {
        return rtrim($this->getCurrentStore()->getBaseUrl(), '/') . '/' . ltrim($path, '/');
    }

    /**
     * @method getStoreLinkUrl
     * @return string
     */
    public function getStoreLinkUrl()
    {
        return $this->getCurrentStore()->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_LINK);
    }

    /**
     * @method getStoreMediaUrl
     * @return string
     */
    public function getStoreMediaUrl()
    {
        return $this->getCurrentStore()->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA);
    }

    /**
     * @method getStoreSkinUrl
     * @return string
     */
    public function getStoreSkinUrl()
    {
        return $this->getCurrentStore()->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_SKIN);
    }

    /**
     * @method getSpeedsizePrefixUrl
     * @param  string $path
     * @param  array $params
     * @return string
     */
    public function getSpeedsizePrefixUrl($path = "", $params = array())
    {
        $params = $params ? '/' . implode(",", $params) : "";
        return $this->getSpeedSizeServiceUrl($this->getSpeedSizeClientId()) . '/' . (string) $path . $params;
    }

    /**
     * @method isSpeedSizeUrl
     * @param  string              $url
     * @return bool
     */
    public function isSpeedSizeUrl($url)
    {
        return strpos($url, self::SPEEDSIZE_SERVICE_URL) === 0;
    }

    /**
     * @method separateSizesFromCacheImageUrl
     * @param  string              $url
     * @return array               $image
     */
    public static function separateSizesFromCacheImageUrl($url)
    {
        $image = array(
            'url' => $url,
            'width' => null,
            'height' => null
        );

        preg_match('/(.+\/)cache\/\d+\/\w+\/(\d{1,4})x(\d{1,4})?\/([a-f0-9]{32})\/(.+)/msi', $url, $matches);
        if ($matches && count($matches) === 6) {
            $image['url'] = $matches[1] . $matches[5];
            $image['width'] = $matches[2];
            $image['height'] = !empty($matches[3]) ? $matches[3] : null;
        }

        return $image;
    }
}
