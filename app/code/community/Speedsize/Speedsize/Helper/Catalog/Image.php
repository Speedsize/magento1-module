<?php
/**
 * SpeedSize module for Magento 1
 *
 * @category SpeedSize
 * @package  Speedsize_Speedsize
 * @author   Developer: Pniel Cohen (Trus)
 * @author   Trus (https://www.trus.co.il/)
 */

/**
 * Speedsize_Speedsize_Helper_Catalog_Image
 */
class Speedsize_Speedsize_Helper_Catalog_Image extends Mage_Catalog_Helper_Image
{
    /**
     * @var Speedsize_Speedsize_Helper_Processor
     */
    protected $speedsizeProcessor;

    /**
     * @return Speedsize_Speedsize_Helper_Processor
     */
    private function getSpeedsizeProcessor()
    {
        return is_null($this->speedsizeProcessor) ? Mage::helper('speedsize_speedsize/processor') : $this->speedsizeProcessor;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        $url = parent::__toString();

        try {
            $speedsizeProcessor = $this->getSpeedsizeProcessor();

            if (!$speedsizeProcessor->canProcess() || $speedsizeProcessor->isSpeedSizeUrl($url) || !$url) {
                return $url;
            }

            $origUrl = Mage::getBaseUrl('media') . str_replace(DS, '/', str_replace(Mage::getBaseDir('media') . DS, "", $this->_getModel()->getBaseFile()));

            $width = (string) $this->_getModel()->getWidth();
            $params = !empty($width) ? array("w_{$width}") : array();
            if ($this->_getModel()->getKeepFrameVal() && $width) {
                $height = (string) $this->_getModel()->getHeight() ?: $width;
                $params[] = "h_{$height}";
                $params[] = "r_contain";
            }

            return $speedsizeProcessor->prefixUrl(
                $origUrl,
                !empty($width) ? array("w_{$width}") : array(),
                false
            );
        } catch (\Exception $e) {
            return $url;
        }
    }
}
