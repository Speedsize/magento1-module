<?php
/**
 * SpeedSize module for Magento 1
 *
 * @category SpeedSize
 * @package  Speedsize_Speedsize
 * @author   Developer: Pniel Cohen (Trus)
 * @author   Trus (https://www.trus.co.il/)
 */

/**
 * SpeedSize Processor helper.
 */
 class Speedsize_Speedsize_Helper_Processor extends Mage_Core_Helper_Abstract
 {
     /**
      * @var array
      */
     private $cache = array();

     /**
      * @var Speedsize_Speedsize_Helper_Data
      */
     private $speedsizeHelper;

     /**
      * @method __construct
      */
     public function __construct()
     {
         $this->speedsizeHelper = Mage::helper('speedsize_speedsize/data');
     }

     /**
      * @return Speedsize_Speedsize_Helper_Data
      */
     public function getSpeedsizeHelper()
     {
         return $this->speedsizeHelper;
     }

     /**
      * Is enabled with active client ID and no bypass?
      * @method canProcess
      * @param  bool    $refresh.
      * @return bool
      */
     public function canProcess($refresh = false)
     {
         if (!isset($this->cache['can_process']) || $refresh) {
             $this->cache['can_process'] =
                $this->speedsizeHelper->isEnabled() &&
                $this->speedsizeHelper->hasActiveCredentials() &&
                !$this->speedsizeHelper->isAdmin() &&
                Mage::app()->getRequest()->getParam(Speedsize_Speedsize_Helper_Data::SPEEDSIZE_BYPASS_QUERY_PARAM) === null;
         }
         return $this->cache['can_process'];
     }

     /**
      * @method isSpeedSizeUrl
      * @param  string              $url
      * @return bool
      */
     public function isSpeedSizeUrl($url)
     {
         return $this->speedsizeHelper->isSpeedSizeUrl($url);
     }

     /**
      * @method prefixUrl
      * @param  string               $url
      * @param  array                $params
      * @param  bool                 $checkIfSpeedSizeUrl
      * @return string
      */
     public function prefixUrl($url = "", $params = array(), $checkIfSpeedSizeUrl = true)
     {
         if ($checkIfSpeedSizeUrl && $this->isSpeedSizeUrl($url)) {
             return $url;
         }
         if (substr($url, 0, 2) === '//') {
             $url = ($this->speedsizeHelper->isCurrentlySecure() ? 'https:' : 'http:') . $url;
         }
         return $this->speedsizeHelper->getSpeedsizePrefixUrl($url, $params);
     }
 }
