# [SpeedSize™](https://www.speedsize.com/) AI Media Optimizer - Magento 1 module

* Neuroscience Image & Video Optimization for Magento 1 websites.
* ~90-99% smaller media, 100% of the visual quality.
* Get Sharper & Faster

---

## Install using Magento Connect manager (from Magento Admin)
1. From your Magento admin menu, navigate to System > Magento Connect > Magento Connect Manager.
2. Scroll to "Direct package file upload" and upload the package file that's included in this repository (var/connect/Speedsize_SpeedSize-\*.tgz).
3. Install the package...

## Install Manually
1. Download/clone this repository & copy/drag the `app` directory into your Magento 1 root dir.
2. Clear your Magento cache from admin (or by manually removing the contents of `{MAGENTO-ROOT-DIR}/var/cache/*` and/or `{MAGENTO-ROOT-DIR}/var/full_page_cache/*` from your server).

## Install Using Modman
1. Make sure you have `modman` installed according to the [official instructions](https://github.com/colinmollenhour/modman#installation) and followed by the [instructions for Magento users](https://github.com/colinmollenhour/modman#magento-users).
2. In case you haven't done it already, open the terminal and run `modman init` from your Magento 1 root dir.
3. From your Magento 1 root dir, run `modman clone Speedsize_SpeedSize https://github.com/speedsize/speedsize_magento`
4. Clear your Magento cache from admin (or by manually removing the contents of `{MAGENTO-ROOT-DIR}/var/cache/*` and/or `{MAGENTO-ROOT-DIR}/var/full_page_cache/*` from your server).

## Update Using Modman
1. Via the terminal and from your Magento 1 root dir, run `modman update Speedsize_SpeedSize`
2. Clear your Magento cache from admin (or by manually removing the contents of `{MAGENTO-ROOT-DIR}/var/cache/*` and/or `{MAGENTO-ROOT-DIR}/var/full_page_cache/*` from your server).

---

https://www.speedsize.com/

![SpeedSize Logo](https://speedsize.com/public/v3/main-page/assets/img/speedsize-logo.svg)
